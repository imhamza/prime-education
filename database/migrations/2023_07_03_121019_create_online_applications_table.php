<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlineApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_applications', function (Blueprint $table) {
            $table->id();
            $table->string('campus_name')->nullable();
//            $table->string('first_name')->nullable();
//            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
//            $table->string('dob')->nullable();
//            $table->string('year')->nullable();
//            $table->string('subject')->nullable();
//            $table->string('other')->nullable();
            $table->string('door_number')->nullable();
            $table->text('street')->nullable();
            $table->string('area')->nullable();
            $table->string('post_code')->nullable();
            $table->string('parent_name')->nullable();
            $table->string('parent_relation')->nullable();
            $table->string('phone')->nullable();
//            $table->string('student_name')->nullable();
//            $table->string('student_id')->nullable();
            $table->string('no_of_student')->nullable();
//            $table->string('relation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_applications');
    }
}
