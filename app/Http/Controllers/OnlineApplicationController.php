<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OnlineApplication;
use App\Models\OnlineApplicationStudent;
use Illuminate\Support\Facades\Mail;

class OnlineApplicationController extends Controller
{
     public function add_subject($id){
        $data = [];
        $var = $id;
        $html = view("pages.online_application.student_fields", compact('data','var'))->render();

        return response(
            [
                'html' => $html,
            ], 200
        );
    }

    public function online_application_store(Request $request)
    {
        //return $request->first_name[0];

        $this->validate($request, [
            /*'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|email',
            'year'=>'required',
            'subject'=>'required'*/
            //'other'=>'required',
            'door_number' => 'required',
            'street' => 'required',
            'area' => 'required',
            'post_code' => 'required',
            'parent_name' => 'required',
            'phone' => 'required',
            'email' => 'email|required',

        ]);
        //return $request->all();
//                dd($request->only('parent_name','email','parent_relation','phone','door_number','street','area','postal_code', 'campus_name','category'));

        $application = OnlineApplication::create($request->only('parent_name','email','parent_relation','phone','door_number','street','area','postal_code', 'campus_name'));
        $no_of_student = count($request->first_name);
        $application->no_of_student = $no_of_student;
        $application->save();

        if (count($request->first_name) > 0) {
            foreach ($request->first_name as $index => $value) {



                    $temp = 'subject_'.$index;     
                    $strConverter =  $request->input($temp);
                if(is_array($strConverter))
                {    
                    $data =  implode(", ",$strConverter);
                }
                else
                {
                    $data = $strConverter;
                }    

                OnlineApplicationStudent::create([
                    'first_name' => $request->first_name[$index],
                    'last_name' => $request->last_name[$index],
                    'year' => $request->year[$index],
                    'subject' => $data,
                    'online_application_id' => $application->id,
                    'course' => $request->course[$index]
                ]);
            }
        }

        $to_name = 'RECEIVER_NAME';
        $to_email = '2015ce8@student.uet.edu.pk';
/*        
        $data = array("name"=>"Ogbonna Vitalis(sender_name)", "body" => "A test mail");
        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name)
        ->subject('Laravel Test Mail');
        $message->from('mahmood.zia.uppal@gmail.com','Test Mail');
    });
*/


//        try {
/*
            Mail::send('emails.online_application_email', ['record' => $application], function ($message) use ($application) {

                $message->subject('Enrollement Enquiry at Prime Education');
                //$message->to( $emailData['to'] , (isset($emailData['toName']) ) ? $emailData['toName'] : '' );
                $message->to('engr.mahmooduppal@gmail.com', 'Prime Education');
*/
//                $message->to('goodblessnoman@gmail.com', 'Prime Tuition');
//                $message->cc('mahmood.zia.uppal@gmail.com', 'Prime Tuition');

//                $message->replyTo($to_email, $to_name);
                /*if( isset($emailData['cc']) && !empty($emailData['cc']) ) {
                $message->cc( $emailData['cc'] , $emailData['ccName'] );
                }*/

                /*if(isset($emailData['bcc']) && !empty($emailData['bcc']) ) {
                $message->cc( $emailData['bcc'] , $emailData['bccName'] );
                }*/
                /*if( !isset($emailData['from']) || empty( $emailData['fromName']) ) {

            $message->from( self::getAdminEmail() , self::getAdminName()  );

            } else {
            $message->from( $emailData['from']  , $emailData['fromName']  );

            }*/

//            });
//        }
//         catch (\Exception $e) 
//         {

//        }
        //return "here";
        //$request->session()->flash('success', 'You application is submitted.');
        return redirect('online_application_thanks');
    }


}
