<?php

namespace App\Http\Controllers;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactFormController extends Controller
{
  
    public function ContactStore(Request $request)
    {
        $request->validate([
            'name'=> 'required',
            'email'=> 'required',
            'subject'=> 'required',
            'message'=> 'required',
            ]);
            Contact::create([
                'name' => $request->name,
                'email' =>$request->email,
                'subject' =>$request->subject,
                'message' =>$request->message,
               ]);
/*            
            return response([
                'status' => true, 
                'message' => "Contact form Submitted!",
            ],201);
*/
            return redirect('/')->with('alert', 'Contact Form Submitted!');

    }

}





