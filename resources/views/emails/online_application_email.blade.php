<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
    <style>

      table
      {
      	width:100%;
      }	
      th
      {
        text-align:left;
        padding: 10px 10px 10px 10px;
        text-indent: -2.5pt;
      }
      tr
      {
        text-align:left;
        padding: 10px 10px 10px 10px;
        text-indent: 4pt;
      } 
      .parenttable th
      {
        text-align:left;
      	width: 50%;
      	text-indent: 2.5pt;
        padding: 2px 2px 2px 2px;
      }

      .parenttable tr{
      	text-align: left;
      	text-indent: 8.5pt;
        padding: 2px 2px 2px 2px;
      }

    </style>    
        <div id="email" style="width:600px;">

			@include('pages.online_application.online_application_details')

		</div>	
</body>
</html>