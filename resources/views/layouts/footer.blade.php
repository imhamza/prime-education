  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-md-4 footer-contact">
            <img src="{{asset('assets/img/logo.png')}}" style="margin: 0px;" height="100px" width="150px">
            <p class="mt-2">
              10 Acre House, Acre Lane <br>
              (Opposite Lambeth Town Hall)<br>
              Brixton London SW2 5SG. <br><br>
              <strong>Phone:</strong> 0203 302 1524<br>
              <strong>Email:</strong> <a href="mailto:info@prime-education.org.uk">info@prime-education.org.uk</a><br>
            </p>
          </div>

          <div class="col-md-3 footer-links">
            <h4>Main Navigation</h4>
            <ul>
            <li class="bx bx-chevron-right"><a href="/" >Home</a></li>
            <li class="bx bx-chevron-right"><a href="{{ route('about-us') }}" >About Us</a></li>
            <li class="bx bx-chevron-right"><a href="{{route('education_benefits')}}"" >Why Prime Education</a></li>
            <li class="bx bx-chevron-right"><a href="{{route('service')}}">Services</a></li>
            <!--
            <li class="bx bx-chevron-right"><a href="" >Awarding Bodies</a></li>
            -->
            <li class="bx bx-chevron-right"><a href="{{route('career')}}" >Careers</a></li>
            <li class="bx bx-chevron-right"><a href="{{route('contact-us')}}" >Contact</a></li></ul>
            </ul>
          </div>

          <div class="col-md-3 footer-links">
            <h4>Admission</h4>
            <ul>
              <li class="bx bx-chevron-right"><a href="" >Programs Offered</a></li>
              <li class="bx bx-chevron-right"><a href="{{ route('online_application') }}" >Apply Online</a></li>
              <li class="bx bx-chevron-right"><a href="" >Fee Information</a></li>
              <li class="bx bx-chevron-right"><a href="" >Week Days Classes</a></li>
              <li class="bx bx-chevron-right"><a href="" >Faqs</a></li></ul>
            </ul>
          </div>

          <div class="col-md-2 footer-links">
            <h4>Learning Tips</h4>
            <ul>
            <li class="bx bx-chevron-right"><a href="" >Mocks Preparation</a></li>
            <li class="bx bx-chevron-right"><a href="" >Covering of Syllabus</a></li>
            <li class="bx bx-chevron-right"><a href="" >Crash Courses</a></li>
            <li class="bx bx-chevron-right"><a href="" >Re Sits</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="container d-md-flex py-3">

      <div class="me-md-auto text-center text-md-start">
        <div class="copyright">
        <p><strong><span>PrimeEducation</span></strong> &copy; 2022. All Rights Reserved<p>
      </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="http://twitter.com/share?text=Prime%20Education%20-%20Careers&url=http%3A%2F%2Fprime-education.org.uk%2Findex.php%2Fcareers" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fprime-education.org.uk%2Findex.php%2Fcareers&t=Prime%20Education%20-%20Careers" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fprime-education.org.uk%2Findex.php%2Fcareers&title=Prime%20Education%20-%20Careers" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
  </footer><!-- End Footer -->
