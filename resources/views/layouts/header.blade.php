<!-- ======= Header ======= -->
<header id="header" class="fixed-top d-flex align-items-center  header-transparent ">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        <h1><a href="{{route('frontend')}}">Prime<span>Education</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="{{route('frontend')}}">Home</a></li>
          <li class="dropdown"><a href="{{ route('about-us') }}"><span>About Us</span></a>
            {{-- <ul>
              <li><a href="{{route('our_mission')}}">Our Mission</a></li>
              <li><a href="{{route('our_vision')}}">Our Vision</a></li>
              <li><a href="{{route('our_team')}}">Our Team</a></li>
            </ul> --}}
          </li>
          <li class="dropdown"><a href="#"><span>Why Prime Education</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="{{route('education_benefits')}}">Education Benefit</a></li>
              <li><a href="{{route('parent_comment')}}">Parent Comments</a></li> 
              <!--
              <li><a href="http://prime-childcare.co.uk">Childcare</a></li> 
              -->
            </ul>
          </li>
          <li><a class="nav-link scrollto" href="{{route('service')}}">Services</a></li>
          <!--
          <li class="dropdown"><a href="#"><span>Awarding Bodies</span> <i class="bi bi-chevron-down"></i></a>
          -->  
            {{-- <ul>
              <li><a href="{{route('ofsted')}}">Ofsted</a></li>
              <li><a href="{{route('ocr')}}">OCR</a></li>
              <li><a href="{{route('edexcel')}}">Edexcel</a></li>
            </ul> --}}
          </li>
          <li><a class="nav-link scrollto" href="{{route('career')}}">Careers</a></li>
          <li><a class="nav-link scrollto" href="{{route('contact-us')}}">Contact</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->
