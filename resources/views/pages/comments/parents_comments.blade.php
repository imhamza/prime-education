@extends('layouts.pages_master')
@section('page_title')
  Why Prime Education  
@endsection
@section('title')
  Parent's Comment
@endsection 
@section('content')
  <section id="testimonials" class="testimonials">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Why Prime Education</h2>
        <p>What Parent's are saying about us</p>
      </div>

      <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
        <div class="swiper-wrapper">
{{--
          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                My Son has Got an Exceptional GCSE Results range from 9 To 7 in all subjects with support of Prime Tuition. Prime Tuition is proved to be excellent in teaching and resources they provide to my child. I highly appreciate them and recommended prime education to other parents.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Caroline</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Excellent in every respect, teaching, conduct, respectful, give attention to details and follow-ups to students work and also very clean environment. Very safe as well. Thank you and keep on the God job.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="">
              <h3>Peterson</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                My daughter joined you in February to prepare for Grammer Schools Selective Exams in Sept. She has never been taught Verbal and Non Verbal Reasoning before and it was difficult for her at the beginning but you were able to break it down for her and she started enjoying it
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="">
              <h3>Anita</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->
--}}

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  I cannot express my gratitude enough for the incredible support and educational opportunities provided by the Prime Education Foundation. They have not only offered extra free lessons to my child in need of support but has also played a significant role in helping him achieve exceptional academic success.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Saida Shirwa</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->


          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Prime Education Foundation is an exceptional organization. They offered half price booster lessons support for my child's A-level exams, and the results were remarkable. The tutors were not only highly qualified but also caring and dedicated. The personalized attention and tailored programs made a significant impact on my child's understanding of the subjects. Prime Education truly goes the extra mile to help students succeed!
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Rose Green</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  I can't thank Prime Education enough for their outstanding assistance during my child's GCSE exams. The they provided were of the highest quality, and the tutors were patient and knowledgeable. My child's grades improved significantly, and their confidence skyrocketed. Prime Education Foundation is a blessing for families who are deprived and can’t afford expensive tuition but want their children to excel academically.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>John Doe</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  I am extremely grateful to Prime Education for their outstanding support during my child's GCSE and A-level exams. The discounted tuition lessons they provided were incredibly beneficial and their tutors were supportive. Thanks to Prime Education's guidance and resources, my child achieved excellent grades that surpassed our expectations. I can't thank them enough for the positive impact they've made on my child's education.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Fang</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->


          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Prime Education has been a tremendous support for my child's SATS and literacy skills development. The free lessons they offer are outstanding and have greatly improved my child's understanding and confidence. The tutors are patient, and dedicated. Prime Education's commitment to providing quality education for all children is commendable.             I highly appreciate Prime Education to support local parents seeking support for their children in SATS and literacy skills.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Helen</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Prime Education Foundation has been a true blessing for me in improving my literacy and numeracy skills. The free programs they offer are comprehensive, engaging, and have greatly boosted my confidence. The tutors are passionate, patient, and dedicated to helping individuals succeed. Thanks to Prime Education, my reading and math abilities have improved significantly. I am grateful for their commitment to empowering individuals through education
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
<!--
              <h3>Helen</h3>
              <h4>Parent</h4>
-->              
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Prime Education has been an incredible resource for adults seeking literacy support in our community. The free programs they offer are outstanding, and the tutors are compassionate, knowledgeable, and skilled at helping adults improve their literacy skills. Thanks to Prime Education, I have gained confidence in reading and writing, which has positively impacted my personal and professional life. I am deeply grateful for their dedication and support. I highly recommend Prime Education to adults seeking literacy assistance.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Paulina</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Prime Education has been a lifeline for adults and parents in our local community seeking literacy and numeracy support. The programs they offer are significantly discounted but comprehensive and tailored to individual needs. The tutors are patient, understanding, and skilled in helping adults improve their literacy skills. Thanks to Prime Education, I have gained confidence in reading and writing, and my overall literacy skills have improved significantly. I am grateful for their dedication to empowering adults through education.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>M Buaro</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->


        </div>
      </div>
    </div>
  </section>
            
@endsection