@extends('layouts.pages_master')
@section('page_title')
  Why Prime Education  
@endsection
@section('title')
  Education Benefits
@endsection 
@section('content')
<section id="about" class="about">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Why Prime Education</h2>
        <p>Education Benefits</p>
        <h6>Our Education is delivered in line with the National Curriculum, this ensures our teaching directly benefits your child's school work. Our teaching is carried out in a fully equipped and supportive environments, free of the pressures children sometimes experience at school.</h6>
      </div>
      <div class="row content" data-aos="fade-up">
        <div class="col-lg-12">
          <h3>Benefits of Education</h3>
          <ul>
            <li><i class="ri-check-double-line"></i><p>Improved performance at school</p></li>
            <li><i class="ri-check-double-line"></i><p>Improved individual performance</p></li>
            <li><i class="ri-check-double-line"></i><p>Improved commitment</p></li>
            <li><i class="ri-check-double-line"></i><p>Motivated Students</p></li>
            <li><i class="ri-check-double-line"></i><p>Supported personal development</p></li>
            <li><i class="ri-check-double-line"></i><p>Course planning</p></li>
          </ul>
             
          <h3>The results of attending Education reported were improvements in:</h3>
          <ul>
            <li><i class="ri-check-double-line"></i><p>Productivity</p></li>
            <li><i class="ri-check-double-line"></i><p>Improved SATs results</p></li>
            <li><i class="ri-check-double-line"></i><p>Higher grades</p></li>
            <li><i class="ri-check-double-line"></i><p>Understanding of exam procedures</p></li>
            <li><i class="ri-check-double-line"></i><p>Increase knowledge base</p></li>
          </ul>

          <h3>Students receiving Education reported improvements in:</h3>

          <ul>
            <li><i class="ri-check-double-line"></i><p>Improved confidence and motivation</p></li>
            <li><i class="ri-check-double-line"></i><p>Improved relationships with school</p></li>
            <li><i class="ri-check-double-line"></i><p>Improved studying technique</p></li>
            <li><i class="ri-check-double-line"></i><p>Improved knowledge base</p></li>
          </ul>
          
          <h3>Research has shown that Education:</h3>
          <ul>
            <li><i class="ri-check-double-line"></i><p>Improve attitudes to school and attendance at school</p></li>
            <li><i class="ri-check-double-line"></i><p>Help to raise self-esteem</p></li>
            <li><i class="ri-check-double-line"></i><p>Helps young people become better learners</p></li>
            <li><i class="ri-check-double-line"></i><p>Helps young people who struggle with motivation at school to become more committed to learning</p></li>
          </ul>
        </div>
      </div>

    </div>
  </section><!-- End About Section -->
@endsection