@extends('layouts.pages_master')
@section('page_title')
  Ofsted 
@endsection
@section('title')
  Ofsted
@endsection 
@section('content')
<section id="about" class="about">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Awarding Body</h2>
        <p>Ofsted Registered</p>
        <h6>Our Centre is an Ofsted registered centre; therefore you can be confident that your child will be educated within a safe and secure environment. All of our students’ details are kept up to date so that we are fully aware of educational everyone’s background and learning needs. We have to ensure all our policies and procedures are fully aligned with the Ofsted regulations.</h6>
      </div>

      <div class="row content" data-aos="fade-up">
        <div class="col-lg-6">
        <h3>Being an Ofsted registered centre assures you that:</h3>
          <ul>
            <li><i class="ri-check-double-line"></i><p>We have met standards designed to safeguard children.</p></li>
            <li><i class="ri-check-double-line"></i><p>Our premises, equipment and care provision is safe and suitable.</p></li>
            <li><i class="ri-check-double-line"></i><p>We have met standards designed to safeguard children.</p></li>
            <li><i class="ri-check-double-line"></i><p>Our premises, equipment and care provision is safe and suitable.</p></li>
            <li><i class="ri-check-double-line"></i><p>All staff with unsupervised access to children have undergone Disclosure and Barring Service (DBS) checks (previously CRB checks).</p></li>
            <li><i class="ri-check-double-line"></i><p>At least one staff member has had First Aid training – At least one staff member has had appropriate training regarding the Common Core Skills.</p></li>
            <li><i class="ri-check-double-line"></i><p>We can accept Childcare Vouchers.</p></li>
            <li><i class="ri-check-double-line"></i><p>Parents may be eligible for help with fees under the Working Tax Credit arrangements.</p></li>
          </ul>
        </div>
      </div>

    </div>
  </section><!-- End About Section -->
@endsection