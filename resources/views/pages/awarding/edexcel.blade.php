@extends('layouts.pages_master')
@section('page_title')
  Edexcel
@endsection
@section('title')
  Edexcel
@endsection 
@section('content')
<section id="about" class="about">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Awarding Body</h2>
        <p>Edexcel</p>
      </div>

      <div class="row content" data-aos="fade-up">
        <div class="col-lg-12">
          <ul>
          <li><i class="ri-check-double-line"></i><p>Prime Education is an approved examination centre for EDEXCEL and help students for the preparation of GCSE & GCE exams to secure good grades in all major subjects. Private Examination Candidates are always welcomed. We offer students the chance sit exams privately to ensure students get top grades!</p></li>
          </ul>
        </div>
      </div>

    </div>
  </section><!-- End About Section -->
@endsection