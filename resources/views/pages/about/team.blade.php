@extends('layouts.pages_master')
@section('page_title')
  About Us  
@endsection
@section('title')
  Our Team
@endsection 
@section('content')
<section id="about" class="about">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>About</h2>
        <p>Our Team</p>
      </div>

      <div class="row content" data-aos="fade-up">
        <div class="col-lg-6">
          <ul>
            <li><i class="ri-check-double-line"></i><p>COMING SOON...</p></li>
          </ul>
        </div>
      </div>

    </div>
  </section><!-- End About Section -->
@endsection
