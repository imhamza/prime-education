@extends('layouts.pages_master')
@section('page_title')
  About Us  
@endsection
@section('title')
  Our Vision
@endsection 
@section('content')
<section id="about" class="about">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>About</h2>
        <p>Our Vision</p>
      </div>

      <div class="row content" data-aos="fade-up">
        <div class="col-lg-12">
          <ul>
            <li><i class="ri-check-double-line"></i><p>Our vision is to be a Tuition centre with excellent level of pedigree with good integrity and reliability in nurturing students to achieve excellence and success.</p></li>
            <li><i class="ri-check-double-line"></i><p> We are an institution run by experienced teachers, graduates, high-achieving undergraduates who are eager to pass on their experiences and expertise. Our school have a quality control program to provide you the professional, prepared tutors who know the school modules inside out. It is Prime Tuition’s dream to until students with the fundamental skills and core knowledge that will reward them their best possible Results in their exams. Present employment opportunities to the graduates and high-achieving undergraduate</li>
            
          </ul>
        </div>
      </div>

    </div>
  </section><!-- End About Section -->
@endsection
