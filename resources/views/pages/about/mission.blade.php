@extends('layouts.pages_master')
@section('page_title')
  About Us  
@endsection
@section('title')
  Our Mission
@endsection 
@section('content')
<section id="about" class="about">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>About</h2>
        <p>Our Mission</p>
      </div>

      <div class="row content" data-aos="fade-up">
        <div class="col-lg-12">
          <ul>
            <li><i class="ri-check-double-line"></i><p>The company mission is to assist and provide cooperation’s with academic schools to improve self-developments and educations to ensure excellent future.</p></li>
            <li><i class="ri-check-double-line"></i><p> Present employment opportunities to the graduates and high-achieving undergraduate</li>
            <li><i class="ri-check-double-line"></i><p> Utilizing the expertise of experience teachers/ officers including retirees to contribute to educational benefits to the institution.</p></li>
            <li><i class="ri-check-double-line"></i><p> Exercising the proper definition of ‘Tuition School’ which is to help those students which are in need of better understanding on their limitations of certain subjects.</p></li>
            <li><i class="ri-check-double-line"></i><p> Provide varieties of teaching and learning tools incorporated with relevant theories applies to the academic purposes.</p></li>
            <li><i class="ri-check-double-line"></i><p> Prioritizing on the accomplishment of our duty to ensure the success of the students with high level of integrity, discipline and honesty rather than profit maximization. Ensuring their parents expenses are valued.</p></li>
          </ul>
        </div>
      </div>

    </div>
  </section><!-- End About Section -->
@endsection
