@extends('layouts.pages_master')
@section('page_title')
  About Us  
@endsection
@section('title')
  About Us 
@endsection 
@section('content')

<section id="about" class="about">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>About Us</h2>
        <p>Our Mission</p>
      </div>

      <div class="row content" data-aos="fade-up">
        <div class="col-lg-12">
          <ul>
            <li><i class="ri-check-double-line"></i><p> Prime Education Foundation is a dedicated education charity committed to empowering and transforming local communities through the provision of high-quality tuition and extra support lessons. Our mission is to create equal opportunities for every individual, ensuring access to quality education regardless of socioeconomic backgrounds.</p></li>
            <li><i class="ri-check-double-line"></i><p> We believe that education is a fundamental right that should not be limited by financial constraints or geographical boundaries. Our foundation aims to bridge the educational gap by offering comprehensive educational support to students of all ages, from primary to secondary level.</li>
            <li><i class="ri-check-double-line"></i><p> At Prime Education Foundation, we are driven by the belief that every child possesses unique talents and abilities waiting to be nurtured. Through our tailored tuition programs, we provide personalized attention, guidance, and resources that enable students to unlock their full potential and excel academically. </p></li>
            <li><i class="ri-check-double-line"></i><p> Our commitment extends beyond traditional classroom learning. We strive to foster a holistic educational experience by offering additional support lessons in critical subjects, study skills, and personal development. By equipping students with essential knowledge, skills, and confidence, we aim to empower them to become lifelong learners and productive members of society.</p></li>
            <li><i class="ri-check-double-line"></i><p> Collaboration is at the heart of our approach. We actively engage with the local community, parents, and volunteers to build a network of support. By fostering strong relationships and creating a sense of belonging, we cultivate an environment where students feel motivated and inspired to succeed.</p></li>
            <li><i class="ri-check-double-line"></i><p> Prime Education Foundation operates with the highest standards of professionalism, integrity, and transparency. We are committed to continually improving our programs, employing qualified and passionate educators, and leveraging the latest educational tools and technologies.</p></li>
            <li><i class="ri-check-double-line"></i><p> Together, we envision a future where education is a catalyst for positive change, breaking the cycle of poverty and empowering individuals to create a brighter tomorrow. Through the Prime Education Foundation, we strive to build a community where every student has the opportunity to thrive, transforming lives one lesson at a time.</p></li>
          </ul>
        </div>
      </div>

      <div class="section-title" data-aos="zoom-out">
        <p>How We Work</p>
      </div>

      <div class="row content" data-aos="fade-up">
        <div class="col-lg-12">
          <ul>
            <li><i class="ri-check-double-line"></i><p> We work with local children, expert tutors and subject heads to create a bespoke plan that meets the needs of the local children community.</p></li>
            <li><i class="ri-check-double-line"></i><p> The target beneficiaries are the children and young people living in the communities where we work.</p></li>
          </ul>
        </div>
      </div>


    </div>
  </section><!-- End About Section -->


@endsection