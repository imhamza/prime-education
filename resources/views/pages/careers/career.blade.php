@extends('layouts.pages_master')
@section('page_title')
  Ofstead 
@endsection
@section('title')
  Careers
@endsection 
@section('content')
<section id="about" class="about">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Careers</h2>
        <p>Careers</p>
        <h6>We are seeking innovative and qualified tutors to join the Mathematics, Science and English faculty at our leading tuition centre. You will be a hard-working and enthusiastic teaching professional with excellent subject knowledge and a sound understanding of the following subjects.</h6>
      </div>

      <div class="row content" data-aos="fade-up">
        <div class="col-lg-12">
          <ul>
            <li><i class="ri-check-double-line"></i><p>1-GCSE/A-Level Mathematics</p></li>
            <li><i class="ri-check-double-line"></i><p>2-GCSE/A-Level Physics</p></li>
            <li><i class="ri-check-double-line"></i><p>3-GCSE/A-Level Chemistry</p></li>
            <li><i class="ri-check-double-line"></i><p>4-GCSE/A-Level English</p></li>
            <li><i class="ri-check-double-line"></i><p>The applicant will be a motivated individual with a keen interest in encouraging and inspiring students to love learning and excel in the subject areas of mathematics, english and science.</p></li>
            <li><i class="ri-check-double-line"></i><p>CRB should definitely be cleared before starting. It is the legal Requirement.</p></li>
          </ul>
          <h4 style="color: #f07014">Please submit your resume and cover letter by email:  <a href="mailto:careers@prime-education.org.uk">careers@prime-education.org.uk</a></h4>
        </div>
      </div>

    </div>
  </section><!-- End About Section -->
@endsection