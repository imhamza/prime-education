@extends('layouts.pages_master')
@section('page_title')
  Online Application  
@endsection
@section('title')
  Online Application 
@endsection 
@section('content')
<style type="text/css">
.button {
	position: relative;
	overflow: hidden;
	display: inline-block;
	padding: 10px 28px;
	font-size: 13px;
	line-height: 1.25;
	border: 2px solid;
	font-family: "Lato", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
	font-weight: 700;
	letter-spacing: 0.05em;
	text-transform: uppercase;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: center;
	cursor: pointer;
	vertical-align: middle;
	user-select: none;
	transition: 250ms all ease-in-out;
}

.button-block {
	display: block;
	width: 100%;
}

@media (min-width: 1200px) {
	.button {
		padding: 13px 50px;
		font-size: 14px;
		line-height: 1.25;
	}
	.button-less {
		padding-left: 30px;
		padding-right: 30px;
	}
	.button-lesser {
		padding-left: 20px;
		padding-right: 20px;
	}
}

.button-default, .button-default:focus {
	color: #cccccc;
	background-color: #292929;
	border-color: #292929;
}

.button-default:hover, .button-default:active {
	color: #ffffff;
	background-color: #fc5b4e;
	border-color: #fc5b4e;
}

.button-default.button-ujarak::before {
	background: #fc5b4e;
}

.button-gray-100, .button-gray-100:focus {
	color: #151515;
	background-color: #f2f3f8;
	border-color: #f2f3f8;
}

.button-gray-100:hover, .button-gray-100:active {
	color: #151515;
	background-color: #e1e4ef;
	border-color: #e1e4ef;
}

.button-gray-100.button-ujarak::before {
	background: #e1e4ef;
}

.button-gray-600, .button-gray-600:focus {
	color: #ffffff;
	background-color: #373737;
	border-color: #373737;
}

.button-gray-600:hover, .button-gray-600:active {
	color: #ffffff;
	background-color: #f07014;
	border-color: #f07014;
}

.button-gray-600.button-ujarak::before {
	background: #f07014;
}

.button-gray-700, .button-gray-700:focus {
	color: #ffffff;
	background-color: #292929;
	border-color: #292929;
}

.button-gray-700:hover, .button-gray-700:active {
	color: #ffffff;
	background-color: #f07014;
	border-color: #f07014;
}

.button-gray-700.button-ujarak::before {
	background: #f07014;
}

.button-primary, .button-primary:focus {
	color: #ffffff;
	background-color: #fc5b4e;
	border-color: #fc5b4e;
}

.button-primary:hover, .button-primary:active {
	color: #ffffff;
	background-color: #f07014;
	border-color: #f07014;
}

.button-primary.button-ujarak::before {
	background: #f07014;
}

.button-primary-light, .button-primary-light:focus {
	color: #ffffff;
	background-color: #f07014;
	border-color: #f07014;
}

.button-primary-light:hover, .button-primary-light:active {
	color: #151515;
	background-color: #ffdf61;
	border-color: #ffdf61;
}

.button-primary-light.button-ujarak::before {
	background: #ffdf61;
}

.button-secondary, .button-secondary:focus {
	color: #151515;
	background-color: #ffdf61;
	border-color: #ffdf61;
}

.button-secondary:hover, .button-secondary:active {
	color: #ffffff;
	background-color: #fc5b4e;
	border-color: #fc5b4e;
}

.button-secondary.button-ujarak::before {
	background: #f07014;
}

.button-secondary-light, .button-secondary-light:focus {
	color: #151515;
	background-color: #ffdf61;
	border-color: #ffdf61;
}

.button-secondary-light:hover, .button-secondary-light:active {
	color: #ffffff;
	background-color: #f07014;
	border-color: #f07014;
}

.button-secondary-light.button-ujarak::before {
	background: #f07014;
}

.button-default-outline {
	border-width: 3px;
}

.button-default-outline, .button-default-outline:focus {
	color: #151515;
	background-color: transparent;
	border-color: rgba(0, 0, 0, 0.2);
}

.button-default-outline:hover, .button-default-outline:active {
	color: #ffffff;
	background-color: #f07014;
	border-color: #f07014;
}

.button-default-outline.button-ujarak::before {
	background: #f07014;
}

.button-ghost {
	padding: 12px 30px;
	color: #151515;
	border: none;
	background-color: #ffffff;
}

.button-ghost:hover {
	color: #ffffff;
	background: #f07014;
}

@media (min-width: 1200px) {
	.button-ghost {
		padding: 15px 52px;
	}
}

.button-facebook, .button-facebook:focus {
	color: #ffffff;
	background-color: #547abb;
	border-color: #547abb;
}

.button-facebook:hover, .button-facebook:active {
	color: #ffffff;
	background-color: #466db0;
	border-color: #466db0;
}

.button-facebook.button-ujarak::before {
	background: #466db0;
}

.button-twitter, .button-twitter:focus {
	color: #ffffff;
	background-color: #44c0f3;
	border-color: #44c0f3;
}

.button-twitter:hover, .button-twitter:active {
	color: #ffffff;
	background-color: #2cb8f1;
	border-color: #2cb8f1;
}

.button-twitter.button-ujarak::before {
	background: #2cb8f1;
}

.button-google, .button-google:focus {
	color: #ffffff;
	background-color: #e75854;
	border-color: #e75854;
}

.button-google:hover, .button-google:active {
	color: #ffffff;
	background-color: #e4423e;
	border-color: #e4423e;
}

.button-google.button-ujarak::before {
	background: #e4423e;
}

.button-shadow {
	box-shadow: 0 9px 21px 0 rgba(204, 204, 204, 0.35);
}

.button-shadow:hover {
	box-shadow: 0 9px 10px 0 rgba(204, 204, 204, 0.15);
}

.button-shadow:focus, .button-shadow:active {
	box-shadow: none;
}

.button-ujarak {
	position: relative;
	z-index: 0;
	transition: background .4s, border-color .4s, color .4s;
}

.button-ujarak::before {
	content: '';
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #fc5b4e;
	z-index: -1;
	opacity: 0;
	transform: scale3d(0.7, 1, 1);
	transition: transform 0.42s, opacity 0.42s;
	border-radius: inherit;
}

.button-ujarak, .button-ujarak::before {
	transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
}

.button-ujarak:hover {
	transition: background .4s .4s, border-color .4s 0s, color .2s 0s;
}

.button-ujarak:hover::before {
	opacity: 1;
	transform: translate3d(0, 0, 0) scale3d(1, 1, 1);
}

.button-xs {
	padding: 8px 17px;
}

.button-sm {
	padding: 8px 32px;
	font-size: 12px;
	line-height: 1.5;
}

.button-lg {
	padding: 15px 67px;
	font-size: 16px;
	line-height: 1.5;
}

.button-xl {
	padding: 16px 50px;
	font-size: 18px;
	line-height: 28px;
}

@media (min-width: 992px) {
	.button-xl {
		padding: 20px 80px;
	}
}

.button-circle {
	border-radius: 30px;
}

.button-round-1 {
	border-radius: 5px;
}

.button-round-2 {
	border-radius: 10px;
}

.button.button-is-icon {
	padding: 5px 19px;
	font-size: 24px;
}

.button.button-icon {
	display: flex;
	justify-content: center;
	align-items: center;
	vertical-align: middle;
}

.button.button-icon .icon {
	position: relative;
	display: inline-block;
	vertical-align: middle;
	color: inherit;
	font-size: 1.55em;
	line-height: 1em;
}

.button.button-icon-left .icon {
	padding-right: 11px;
}

.button.button-icon-right {
	flex-direction: row-reverse;
}

.button.button-icon-right .icon {
	padding-left: 11px;
}

.button.button-icon.button-link .icon {
	top: 5px;
	font-size: 1em;
}

.button.button-icon.button-xs .icon {
	top: .05em;
	font-size: 1.2em;
	padding-right: 8px;
}

.button.button-icon.button-xs .button-icon-right {
	padding-left: 8px;
}

.btn-primary {
	border-radius: 3px;
	font-family: "Lato", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
	font-weight: 700;
	letter-spacing: .05em;
	text-transform: uppercase;
	transition: .33s;
}

.btn-primary, .btn-primary:active, .btn-primary:focus {
	color: #ffffff;
	background: #fc5b4e;
	border-color: #fc5b4e;
}

.btn-primary:hover {
	color: #ffffff;
	background: #000000;
	border-color: #000000;
}

.button-group {
	transform: translate3d(0, -15px, 0);
	margin-bottom: -15px;
	margin-left: -8px;
	margin-right: -8px;
}

.button-group > li {
	display: inline-block;
}

.button-group > * {
	margin-top: 15px;
	padding-left: 8px;
	padding-right: 8px;
}

* + .button {
	margin-top: 10px;
}

* + .button-group {
	margin-top: 15px;
}

@media (min-width: 1200px) {
	* + .button {
		margin-top: 30px;
	}
	* + .button-group {
		margin-top: 30px;
	}
}
.button {
	position: relative;
	overflow: hidden;
	display: inline-block;
	padding: 10px 28px;
	font-size: 13px;
	line-height: 1.25;
	border: 2px solid;
	font-family: "Lato", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
	font-weight: 700;
	letter-spacing: 0.05em;
	text-transform: uppercase;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-align: center;
	cursor: pointer;
	vertical-align: middle;
	user-select: none;
	transition: 250ms all ease-in-out;
}

.button-block {
	display: block;
	width: 100%;
}

@media (min-width: 1200px) {
	.button {
		padding: 13px 50px;
		font-size: 14px;
		line-height: 1.25;
	}
	.button-less {
		padding-left: 30px;
		padding-right: 30px;
	}
	.button-lesser {
		padding-left: 20px;
		padding-right: 20px;
	}
}

.button-default, .button-default:focus {
	color: #cccccc;
	background-color: #292929;
	border-color: #292929;
}

.button-default:hover, .button-default:active {
	color: #ffffff;
	background-color: #fc5b4e;
	border-color: #fc5b4e;
}

.button-default.button-ujarak::before {
	background: #fc5b4e;
}

.button-gray-100, .button-gray-100:focus {
	color: #151515;
	background-color: #f2f3f8;
	border-color: #f2f3f8;
}

.button-gray-100:hover, .button-gray-100:active {
	color: #151515;
	background-color: #e1e4ef;
	border-color: #e1e4ef;
}

.button-gray-100.button-ujarak::before {
	background: #e1e4ef;
}

.button-gray-600, .button-gray-600:focus {
	color: #ffffff;
	background-color: #373737;
	border-color: #373737;
}

.button-gray-600:hover, .button-gray-600:active {
	color: #ffffff;
	background-color: #f07014;
	border-color: #f07014;
}

.button-gray-600.button-ujarak::before {
	background: #f07014;
}

.button-gray-700, .button-gray-700:focus {
	color: #ffffff;
	background-color: #292929;
	border-color: #292929;
}

.button-gray-700:hover, .button-gray-700:active {
	color: #ffffff;
	background-color: #f07014;
	border-color: #f07014;
}

.button-gray-700.button-ujarak::before {
	background: #f07014;
}

.button-primary, .button-primary:focus {
	color: #ffffff;
	background-color: #fc5b4e;
	border-color: #fc5b4e;
}

.button-primary:hover, .button-primary:active {
	color: #ffffff;
	background-color: #f07014;
	border-color: #f07014;
}

.button-primary.button-ujarak::before {
	background: #f07014;
}

.button-primary-light, .button-primary-light:focus {
	color: #ffffff;
	background-color: #f07014;
	border-color: #f07014;
}

.button-primary-light:hover, .button-primary-light:active {
	color: #151515;
	background-color: #ffdf61;
	border-color: #ffdf61;
}

.button-primary-light.button-ujarak::before {
	background: #ffdf61;
}

.button-secondary, .button-secondary:focus {
	color: #151515;
	background-color: #ffdf61;
	border-color: #ffdf61;
}

.button-secondary:hover, .button-secondary:active {
	color: #ffffff;
	background-color: #fc5b4e;
	border-color: #fc5b4e;
}

.button-secondary.button-ujarak::before {
	background: #fc5b4e;
}

.button-secondary-light, .button-secondary-light:focus {
	color: #151515;
	background-color: #ffdf61;
	border-color: #ffdf61;
}

.button-secondary-light:hover, .button-secondary-light:active {
	color: #ffffff;
	background-color: #f07014;
	border-color: #f07014;
}

.button-secondary-light.button-ujarak::before {
	background: #f07014;
}

.button-default-outline {
	border-width: 3px;
}

.button-default-outline, .button-default-outline:focus {
	color: #151515;
	background-color: transparent;
	border-color: rgba(0, 0, 0, 0.2);
}

.button-default-outline:hover, .button-default-outline:active {
	color: #ffffff;
	background-color: #f07014;
	border-color: #f07014;
}

.button-default-outline.button-ujarak::before {
	background: #f07014;
}

.button-ghost {
	padding: 12px 30px;
	color: #151515;
	border: none;
	background-color: #ffffff;
}

.button-ghost:hover {
	color: #ffffff;
	background: #f07014;
}

@media (min-width: 1200px) {
	.button-ghost {
		padding: 15px 52px;
	}
}

.button-facebook, .button-facebook:focus {
	color: #ffffff;
	background-color: #547abb;
	border-color: #547abb;
}

.button-facebook:hover, .button-facebook:active {
	color: #ffffff;
	background-color: #466db0;
	border-color: #466db0;
}

.button-facebook.button-ujarak::before {
	background: #466db0;
}

.button-twitter, .button-twitter:focus {
	color: #ffffff;
	background-color: #44c0f3;
	border-color: #44c0f3;
}

.button-twitter:hover, .button-twitter:active {
	color: #ffffff;
	background-color: #2cb8f1;
	border-color: #2cb8f1;
}

.button-twitter.button-ujarak::before {
	background: #2cb8f1;
}

.button-google, .button-google:focus {
	color: #ffffff;
	background-color: #e75854;
	border-color: #e75854;
}

.button-google:hover, .button-google:active {
	color: #ffffff;
	background-color: #e4423e;
	border-color: #e4423e;
}

.button-google.button-ujarak::before {
	background: #e4423e;
}

.button-shadow {
	box-shadow: 0 9px 21px 0 rgba(204, 204, 204, 0.35);
}

.button-shadow:hover {
	box-shadow: 0 9px 10px 0 rgba(204, 204, 204, 0.15);
}

.button-shadow:focus, .button-shadow:active {
	box-shadow: none;
}

.button-ujarak {
	position: relative;
	z-index: 0;
	transition: background .4s, border-color .4s, color .4s;
}

.button-ujarak::before {
	content: '';
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #fc5b4e;
	z-index: -1;
	opacity: 0;
	transform: scale3d(0.7, 1, 1);
	transition: transform 0.42s, opacity 0.42s;
	border-radius: inherit;
}

.button-ujarak, .button-ujarak::before {
	transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
}

.button-ujarak:hover {
	transition: background .4s .4s, border-color .4s 0s, color .2s 0s;
}

.button-ujarak:hover::before {
	opacity: 1;
	transform: translate3d(0, 0, 0) scale3d(1, 1, 1);
}

.button-xs {
	padding: 8px 17px;
}

.button-sm {
	padding: 8px 32px;
	font-size: 12px;
	line-height: 1.5;
}

.button-lg {
	padding: 15px 67px;
	font-size: 16px;
	line-height: 1.5;
}

.button-xl {
	padding: 16px 50px;
	font-size: 18px;
	line-height: 28px;
}

@media (min-width: 992px) {
	.button-xl {
		padding: 20px 80px;
	}
}

.button-circle {
	border-radius: 30px;
}

.button-round-1 {
	border-radius: 5px;
}

.button-round-2 {
	border-radius: 10px;
}

.button.button-is-icon {
	padding: 5px 19px;
	font-size: 24px;
}

.button.button-icon {
	display: flex;
	justify-content: center;
	align-items: center;
	vertical-align: middle;
}

.button.button-icon .icon {
	position: relative;
	display: inline-block;
	vertical-align: middle;
	color: inherit;
	font-size: 1.55em;
	line-height: 1em;
}

.button.button-icon-left .icon {
	padding-right: 11px;
}

.button.button-icon-right {
	flex-direction: row-reverse;
}

.button.button-icon-right .icon {
	padding-left: 11px;
}

.button.button-icon.button-link .icon {
	top: 5px;
	font-size: 1em;
}

.button.button-icon.button-xs .icon {
	top: .05em;
	font-size: 1.2em;
	padding-right: 8px;
}

.button.button-icon.button-xs .button-icon-right {
	padding-left: 8px;
}

.btn-primary {
	border-radius: 3px;
	font-family: "Lato", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
	font-weight: 700;
	letter-spacing: .05em;
	text-transform: uppercase;
	transition: .33s;
}

.btn-primary, .btn-primary:active, .btn-primary:focus {
	color: #ffffff;
	background: #fc5b4e;
	border-color: #fc5b4e;
}

.btn-primary:hover {
	color: #ffffff;
	background: #000000;
	border-color: #000000;
}

.button-group {
	transform: translate3d(0, -15px, 0);
	margin-bottom: -15px;
	margin-left: -8px;
	margin-right: -8px;
}

.button-group > li {
	display: inline-block;
}

.button-group > * {
	margin-top: 15px;
	padding-left: 8px;
	padding-right: 8px;
}

* + .button {
	margin-top: 10px;
}

* + .button-group {
	margin-top: 15px;
}

@media (min-width: 1200px) {
	* + .button {
		margin-top: 30px;
	}
	* + .button-group {
		margin-top: 30px;
	}
}


</style> 
 


<script src="{{ asset('assets/js/code.jquery.com_jquery-3.7.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendor/select2-develop/dist/js/select2.min.js') }}"></script>
<link href="{{ asset('assets/vendor/select2-develop/dist/css/select2.min.css') }}" rel="stylesheet">
<script type="text/javascript">
      $('.select2').select2();
</script>
<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
 
<section class="section section-lg bg-default">
        <div class="container">
			<h4>Select Campus</h4>
			<form action="{{url('online_application')}}" method="post">
				{!!csrf_field()!!}
            <div class="row">
                 <div class="col-md-6">
						<div class="form-group p-2">
							<label class="p-2">Select Campus<span style="color: red">*</span></label>
							<select class="form-control p-2" name="campus_name">
                                 <option value="Brixton">Brixton</option>
                                 {{--<option value="Hounslow">Hounslow</option>--}}
                                 <option value="Woolwich">Woolwich</option>
                            </select>
						</div>
					</div>
				</div>
                <h4>Student Details</h4>
				<div id="student_details">
					<div  class="row" style="margin-top: 10px">
						<div class="col-lg-12" align="right">

							<button type="button" class="remove_content pull-right float-right text-right button button-primary-light button-round-2 p-2"><i class="bi bi-dash h1" style="font-color:white; font-weight: bolder ;font-size: 18px;"></i></button>

						</div>
						<div class="col-md-6">
							<div class="form-group p-2">
								<label class="p-2">First Name <span style="color: red">*</span></label>
								<input type="text" name="first_name[]" required=""  class="form-control p-2">
							</div>
						</div>
	                    <div class="col-md-6">
							<div class="form-group p-2">
								<label class="p-2">Last Name<span style="color: red">*</span></label>
								<input type="text" name="last_name[]" required="" class="form-control p-2">

							</div>
						</div>
	                    
	                    
						<div class="col-md-6">
						<div class="form-group p-2">
							<label class="p-2">Year in School{{--<span style="color: red">*</span>--}}</label>
							<select class="form-control p-2" required="" name="year[]">
                                 <option value="reception">Reception</option>
                                 <option value="Year_1">Year 1</option>
                                 <option value="Year_2">Year 2</option>
                                 <option value="Year_3">Year 3</option>
                                 <option value="Year_4">Year 4</option>
                                 <option value="Year_5">Year 5</option>
                                 <option value="Year_6">Year 6</option>
                                 <option value="Year_7">Year 7</option>
                                 <option value="Year_8">Year 8</option>
                                 <option value="Year_9">Year 9</option>
                                 <option value="Year_10">Year 10</option>
                                 <option value="Year_11">Year 11</option>
                                 <option value="Year_12">Year 12</option>
                                 <option value="Year_13">Year 13</option>
                                 <option value="Adult">Adult</option>
                                 <option value="Other Year">Other Year</option>
                            </select>
						</div>
					</div>

	                    <div class="col-md-6">
							<div class="form-group p-2">

								<label class="p-2">Subject<span style="color: red">*</span></label>
								<select name="subject_0[]" required="" multiple="" class="select2 form-control p-2">
									
									<option value="math">Math</option>
									<option value="english">English</option>
									<option value="science">Science</option>
								</select>

							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group p-2">
								<label class="p-2">Select Course<span style="color: red">*</span></label>
								<select class="form-control p-2" name="course[]">
									 <option value="Reading Club">Reading Club</option>
									 <option value="Summer School">Summer School</option>
									 <option value="GCSE">GCSE</option>
									 <option value="A Level">A Level</option>
									 <option value="KS1 Tuition">KS1 Tuition</option>
									 <option value="KS2 Tuition">KS2 Tuition</option>
									 <option value="KS2 Writing">KS2 Writing</option>
									 <option value="KS3 Literacy">KS3 Literacy</option>
									 <option value="KS3 Tuition">KS3 Tuition</option>
									 <option value="SATS">SATS</option>									 
								</select>
							</div>
						</div>
	                    
					</div>
				</div>
				
				<div class="new_student">
					
				</div>

                <button type="button" class="button button-primary-light button-round-2 mt-2 p-" onclick="add_new_student()"><span class="fa fa-plus"></span> Add More child</button>

                <h4 class="mt-2 p-2">Parent/Guardian Details</h4>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group p-2">
							<label class="p-2">Parent/Guardian Name</label><span style="color: red">*</span></label>
							<input type="text" name="parent_name" value="{{old('parent_name')?old('parent_name'):@$online_application->parent_name}}" class="form-control p-2">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group p-2">
							<label class="p-2">Email</label><span style="color: red">*</span></label>
							<input type="email" name="email" value="{{old('email')?old('email'):@$online_application->email}}" class="form-control p-2">
						</div>
					</div>
                    <div class="col-md-6">
						<div class="form-group p-2">
							<label class="p-2">Parent/Guardian Relation</label>
							<input type="text" name="parent_relation" value="{{old('parent_relation')?old('parent_relation'):@$online_application->parent_relation}}" class="form-control p-2">
						</div>
					</div>
                    <div class="col-md-6">
						<div class="form-group p-2">
							<label class="p-2">Mobile Number</label><span style="color: red">*</span></label>
							<input type="text" name="phone" id="phone" value="{{old('phone')?old('phone'):@$online_application->phone}}" class="form-control p-2">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group p-2">
							<label class="p-2">Door Number<span style="color: red">*</span></label>
							<input type="text" name="door_number" value="{{old('door_number')?old('door_number'):@$online_application->door_number}}" class="form-control p-2">
						</div>
					</div>
                    <div class="col-md-6">

						<div class="form-group p-2">
							<label class="p-2">Street<span style="color: red">*</span></label>
							<input type="text" name="street" value="{{old('street')?old('street'):@$online_application->street}}" class="form-control p-2">
						</div>
					</div>
                    <div class="col-md-6">
						<div class="form-group p-2">
							<label class="p-2">Area/Guarded<span style="color: red">*</span></label>
							<input type="text" name="area" value="{{old('area')?old('area'):@$online_application->area}}" class="form-control p-2">
						</div>
					</div>
                    <div class="col-md-6">
						<div class="form-group p-2">
							<label class="p-2">Post code<span style="color: red">*</span></label>
							<input type="text" name="post_code" value="{{old('post_code')?old('post_code'):@$online_application->post_code}}" class="form-control p-2">
						</div>
					</div>
				</div>
	             
				
                 <div class="row">
                 <div class="col-md-6">
						<div class="form-group p-2">
							
							<input type="submit" name="submit" class="button button-primary-light button-circle button-shadow mt-2" value="Apply">
						</div>
					</div>
				</div>
			</form>
        </div>
      </section>
<script src="{{ asset('assets/js/code.jquery.com_jquery-3.7.0.min.js') }}"></script>

<script type="text/javascript">
     var x=0;

	function other_relative(bit) {
		if (bit) {
			$('#other_relative_div').show();
		}else{
			$('#other_relative_div').hide();
		}
	}
	$(document).on('click', '.remove_content', function(event) {
		$(this).parent().parent().remove();
		x--;
	});

	function add_new_student() {
		x++;
		$('.select2').select2();
		//$('.new_student').append('<button type="button" class="remove_content btn btn-btn pull-right">Remove</button>');
		
		$('.remove_content').show();
		$('.remove_content:first').hide();

		$.ajax({
		url: '{{url("get_student_fields")}}' + "/" + x,
		
		})
		.done(function(response) {
			$('.new_student').append(response.html);
			$('.select2').select2();
		})
		.fail(function(response) {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}
		$('.remove_content').hide();

	
	
	
</script>

@endsection

@section('javascript')


<script type="text/javascript" src="{{asset('masked_input.js')}}"></script>
<script>
$("#phone").mask("99999 999999");

</script>
@endsection