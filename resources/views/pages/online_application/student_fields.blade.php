				<div >
					<div  class="row" style="margin-top: 10px">
						<div class="col-lg-12 " align="right">

							<button type="button" class="remove_content text-right pull-right float-right button button-primary-light button-round-2"><i class="bi bi-dash h1" style="font-color:white; font-weight: bolder; ;font-size: 18px; "></i></button>

						</div>
						<div class="col-md-6">
							<div class="form-group p-2">
								<label class="p-2">First Name <span style="color: red">*</span></label>
								<input type="text" name="first_name[]" required=""  class="form-control p-2">
							</div>
						</div>
	                    <div class="col-md-6">
							<div class="form-group p-2">
								<label class="p-2">Last Name<span style="color: red">*</span></label>
								<input type="text" name="last_name[]" required="" class="form-control p-2">

							</div>
						</div>
	                    
	                    
						<div class="col-md-6">
						<div class="form-group p-2">
							<label class="p-2">Year in School{{--<span style="color: red">*</span>--}}</label>
							<select class="form-control p-2" required="" name="year[]">
                                 <option value="reception">Reception</option>
                                 <option value="Year_1">Year 1</option>
                                 <option value="Year_2">Year 2</option>
                                 <option value="Year_3">Year 3</option>
                                 <option value="Year_4">Year 4</option>
                                 <option value="Year_5">Year 5</option>
                                 <option value="Year_6">Year 6</option>
                                 <option value="Year_7">Year 7</option>
                                 <option value="Year_8">Year 8</option>
                                 <option value="Year_9">Year 9</option>
                                 <option value="Year_10">Year 10</option>
                                 <option value="Year_11">Year 11</option>
                                 <option value="Year_12">Year 12</option>
                                 <option value="Year_13">Year 13</option>
                                 <option value="Adult">Adult</option>
                                 <option value="Other Year">Other Year</option>
                            </select>
						</div>
					</div>
	                    <div class="col-md-6">
							<div class="form-group p-2">

								<label class="p-2">Subject<span style="color: red">*</span></label>
								<select name="subject_{{$var}}[]" required="" multiple="" class="form-control p-2 select2">
									
									<option value="math">Math</option>
									<option value="english">English</option>
									<option value="science">Science</option>
								</select>

							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group p-2">
								<label class="p-2">Select Course</label>
								<select class="form-control p-2" name="course[]">
									 <option value="Reading Club">Reading Club</option>
									 <option value="11+">11+</option>
									 <option value="Summer School">Summer School</option>
									 <option value="GCSE">GCSE</option>
									 <option value="A Level">A Level</option>
									 <option value="KS1 Tuition">KS1 Tuition</option>
									 <option value="KS2 Tuition">KS2 Tuition</option>
									 <option value="KS2 Writing">KS2 Writing</option>
									 <option value="KS3 Literacy">KS3 Literacy</option>
									 <option value="KS3 Tuition">KS3 Tuition</option>
									 <option value="SATS">SATS</option>						
								</select>
							</div>
						</div>
	                    
					</div>
				</div>