@extends('layouts.pages_master')
@section('page_title')
  Online Application  
@endsection
@section('title')
  Online Application 
@endsection 
@section('content')

<section class="section section-lg bg-default text-center text-white" style="background:  linear-gradient(45deg, violet 0%, blue 50%, purple 100%); height: 400px ">
<div class="container" data-aos="zoom-in-left" data-aos-delay="200">
          <img src="{{ asset('assets/img/success.png') }}" width="200px" height="200px" />
          <br/>
          <br/>
          <h3 style="color:white;">Thank you for your interest in Prime Tuition</h3>
          <h6 class="offset-top-1">We will be in touch with you shortly.</h6>
{{--
          <div class="group-xl group-middle">
          <h5 class="offset-top-1">In the meantime, click here to Download our Leaflet.</h5>
            <div class="button button-ghost button-circle button-shadow" ><a href="{{url('prime_tuition_pdfs/1Prime Tutition 3 Folding.pdf')}}" target="_blank">Download Our Leaflet</a></div>
          </div>
--}}          
        </div>
        
      </section>
{{--
      <section class="section section-md bg-default ">
        
      </section>
--}}      
@endsection

@section('javascript')
<!-- Event snippet for Submit lead form conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-10844284424/L5KoCMDZzacDEIjM-rIo'});
</script>
@endsection