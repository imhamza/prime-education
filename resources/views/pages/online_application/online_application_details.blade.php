<h3><u>Parent Details</u></h3>

                                    <table class="table table-bordered parenttable"  role="presentation" border="2" cellspacing="0" width="100%">
                                        <tbody >
                                            <tr>
                                                <th style="text-align: left; text-indent: 2.5pt;
">Parent Name</th>
                                                <td style="text-indent: 1pt;">{{$record->parent_name}}</td>   
                                            </tr>    

                                            <tr>
                                                <th style="text-align: left; text-indent: 2.5pt;
">Campus Name</th>
                                                <td style="text-indent: 1pt;">{{$record->campus_name}}</td>   
                                            </tr>
                                            <tr>
                                                <th style="text-align: left; text-indent: 2.5pt;
">Parent/Guardian Relation</th>
                                                <td style="text-indent: 1pt;">{{$record->parent_relation}}</td>   

                                            </tr>
                                            <tr>    
                                                <th style="text-align: left; text-indent: 2.5pt;
">Phone</th>
                                                <td style="text-indent: 1pt;">{{$record->phone}}</td>   

                                            </tr>

                                            <tr>
                                                <th style="text-align: left; text-indent: 2.5pt;
">Email</th>
                                                <td style="text-indent: 1pt;">{{$record->email}}</td>   
                                            </tr>
                                            <tr>
                                                <th style="text-align: left; text-indent: 2.5pt;
">Area/Guarded</th>
                                                <td style="text-indent: 1pt;">{{$record->area}}</td>   
                                            </tr>
                                            <tr>
                                                <th style="text-align: left; text-indent: 2.5pt;
">Date Applied</th>                                            
                                                <td style="text-indent: 1pt;">{{ date('d/m/Y',strtotime($record->created_at)) }}</td>
                                            </tr>    
                                        </tbody>
                                    </table>
                                <div>
                                    <h3><u>Students Details </u></h3>
                                    <label>Total <small>({{$record->students->count()}})</small></label>
                                </div>
                                <br/>    
<!--                                    
                                @foreach($record->students as $key => $student)

                                    <table class="table table-hover normal-table" role="presentation" border="2" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Year</th>
                                            <th>Suject</th>
                                            <th>Course</th>
                                            
                                            
                                            

                                        </tr>
                                        </thead>
                                        <tbody>
                                        

                                            <tr class="sub-row " >

                                                
                                                
                                                
                                                <td>{{ $student->first_name }} </td>
                                                <td>{{ $student->last_name }}</td>
                                                <td>{{ $student->year }}</td>
                                                <td>{{ $student->subject }}</td>
                                                <td>{{ $student->category }}</td>

                                           
                                                
                                                
                                            </tr>
                                        
                                        </tbody>
                                    </table>
                                </br>
                                @endforeach
-->                                
                                    <table class="table table-hover normal-table" role="presentation" border="2" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Year</th>
                                            <th>Suject</th>
                                            <th>Course Name</th>
                                            
                                            
                                            

                                        </tr>
                                        </thead>
                                        <tbody>
                                        
                                @foreach($record->students as $key => $student)

                                            <tr class="sub-row " >

                                                
                                                
                                                
                                                <td style="text-indent: 1pt;">{{ $student->first_name }} </td>
                                                <td style="text-indent: 1pt;">{{ $student->last_name }}</td>
                                                <td style="text-indent: 1pt;">{{ $student->year }}</td>
                                                <td style="text-indent: 1pt;">{{ $student->subject }}</td>
                                                <td style="text-indent: 1pt;">{{ $student->course }}</td>

                                                
                                                
                                            </tr>
                                @endforeach                                           
                                        
                                        </tbody>
                                    </table>
