@extends('layouts.pages_master')
@section('page_title')
  Services  
@endsection
@section('title')
  Services
@endsection 
@section('content')
<section id="about" class="about">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Prime Education Services</h2>
        <p>Services</p>
      </div>

      <div class="row content" data-aos="fade-up">
        <div class="col-lg-12">
          <h3>Tuition and Exam Coaching</h3>
          <ul>
            <li><i class="ri-check-double-line"></i><p>Tuition and Exam Coaching Support based at Prime Education Campus. Tuition available for all ages and abilities including GCSE and A Level.</p></li>
          </ul>
          <h3>Free Booster Lessons to improve Exam preparation</h3>
          <ul>
            <li><i class="ri-check-double-line"></i><p>Our Revision Schools help small groups of A Level and GCSE students prepare for upcoming exams and improve performance. Each session is tailored to the subject, exam board and individual student’s requirements. Sessions are run by our qualified tutors.</p></li>
          </ul>
          <h3>Support for additional need learners</h3>
          <ul>
            <li><i class="ri-check-double-line"></i><p>Students with additional needs may include range if situations and circumstances like;
            </li>
            <li><i class="ri-check-line" style=" margin-left: 40px;"></i>
            &emsp;&emsp;Have Learning difficulty
            </li>
            <li><i class="ri-check-line" style=" margin-left: 40px;"></i>
            &emsp;&emsp;
            Speak English as second language
            </li>
            <li><i class="ri-check-line" style=" margin-left: 40px;"></i>
            &emsp;&emsp;
            Are having other additional needs, (social ,emotional, financial)
            </li>
            <li><i class="ri-check-line"style=" margin-left: 40px;"></i>
            &emsp;&emsp;
            Not able to get a school/college place
            </li>
            <li>
            <i class="ri-check-double-line"></i>Supporting children and young people with a range of additional needs. These tailored, intensive programmes help young people aged 8 – 21 to fulfil their potential.
            </li>
            <li> 
            <i class="ri-check-double-line"></i>This can include behaviour or special educational needs or disabilities (SEND), English as an additional language (EAL), looked-after children or care leavers.  We also work with those who are, or are at risk of becoming, NEET (not in education, employment or training).
            </li>
            <li>
            <i class="ri-check-double-line"></i>We also provide one-to-one support to young learners to develop their confidence and motivation.<br/>
            Our programmes help learners to develop skills and confidence, progress in education or move into work or apprenticeships. This can include achieving accredited qualifications.

            <i class="ri-check-double-line"></i>This programme provide support at individual level for each student.</li>
            <li><i class="ri-check-double-line"></i><b>Offered at less than half price</b></p></li>
          </ul>
          <h3>Building Vocabulary in KS1, KS2</h3>
          <ul>
            <li><i class="ri-check-double-line"></i><p>Pupils’ vocabulary affects their ability to communicate and to understand the world.
            </li>
            <li><i class="ri-check-double-line"></i>  
            This course helps students develop their knowledge of how words work and implementation in with group activities. There will also be an exploration of how to foster a language-rich classroom.
            </li>
            <li><i class="ri-check-double-line"></i>
            100% of participating students said they would recommend these lessons to another school.
            </li>
            <li><i class="ri-check-double-line"></i>
            How long is the lesson last?
            </li>
            <li><i class="ri-check-line"></i>
            This course consists of 6 weeks lessons of 2 hours.
            </li>
            <li><i class="ri-check-double-line"></i>
            What will I learn?
            </li>
            <li><i class="ri-check-line" style=" margin-left: 40px;"></i>
            &emsp;&emsp;
            Reading, vocabulary and how they work together.
            </li>
            <li><i class="ri-check-line" style=" margin-left: 40px;"></i>
            &emsp;&emsp; 
            Word meaning, word knowledge and word-building
            </li>
            <li><i class="ri-check-line" style=" margin-left: 40px;"></i>
            &emsp;&emsp; 
            Learning and loving new words – direct and indirect instructions
            </li>
            <li><i class="ri-check-line" style=" margin-left: 40px;"></i>
            &emsp;&emsp;
            Practical strategies and learning reflections
            </li>
            <li><i class="ri-check-double-line"></i>
            <b>Up to 60% discounted fees</b>
            </p></li>
          </ul>
          <h3>Free SATS Lessons</h3>
          <ul>
            <li><i class="ri-check-double-line"></i><p>At Prime Education, your child will progress through the individualized instruction at his or her own pace – helping students achieve target scores in Literacy and Numeracy</p></li>
          </ul>
          <h3>Adult Literacy Support Programme</h3>
          <ul>
            <li><i class="ri-check-double-line"></i><p>This programme help adults and especially to improve their literacy and mathematical skill to improve their communication skills and help them build confidence in society.</p></li>
          </ul>
          <h3>Improving Writing in Key Stage 2</h3>
          <ul>
            <li><i class="ri-check-double-line"></i>This course helps students develop approaches to take their Key Stage 2 writing skill beyond standard writing.
            </li>
            <li><i class="ri-check-double-line"></i>  
            Pupils will build confidence, improve self-assessment skills, and learn how to tackle writing challenges across different text types.
            </li>
            <li><i class="ri-check-double-line"></i><p>"Participating students made approximately six months' additional progress compared to similar pupils who did not participate."</p></li>
          </ul>
          <h3>Mentoring programmes</h3>
          <ul>
            <li><i class="ri-check-double-line"></i><p>"Many children in the UK are not getting the right start in life, leaving them unable to realise their full potential. Children living in poverty will be 4 months behind their peers when they start school. This gap widens over time. On average these children are up to 2 years behind their peers by the time they leave secondary school.
            </li>
            <li>  
            <i class="ri-check-double-line"></i>These lessons include, a learning programme which helps pupils a transition from primary to secondary school and build their ability to gain improved scores by end of secondary school. This has been developed in partnership with Prime Tuition.
            Our mentoring projects provide one-to-one support to young people to develop their confidence and motivation.
            "</p></li>
          </ul>
          <h3>Key Stages 3 Literacy Project</h3>
          <ul>
            <li><i class="ri-check-double-line"></i><p>"Literacy is a fundamental skill that enables students to successfully progress through school, and they can also achieve success in advancing from primary to secondary school and onto adulthood and employment.</li>
            <li><i class="ri-check-double-line"></i>The project Literacy aims to enhance the reading and writing proficiency of all children aged 11–14 years old."</p></li>
          </ul>

        </div>
      </div>

    </div>
  </section><!-- End About Section -->
@endsection