@extends('layouts.app')
@section('title')
    Home
@endsection
@section('content')
<!-- ======= About Section ======= -->
<section id="about" class="about">
    <div class="container">
@if (session('alert'))
<div class="alert alert-success alert-dismissible fade show" id="myalert" role="alert">
  <strong>Success!</strong>         {{ session('alert') }}
</div>
@endif
      <div class="section-title" data-aos="zoom-out">
        <h2>About</h2>
        <p>Who we are</p>
      </div>

      <div class="row content" data-aos="fade-up">
        <div class="col-lg-6 pt-4 pt-lg-0">
          <p style="text-align: justify">
            Prime Education Foundation is a dedicated education charity committed to empowering and transforming local communities through the provision of high-quality tuition and extra support lessons. Our mission is to create equal opportunities for every individual, ensuring access to quality education regardless of socioeconomic backgrounds.
We believe that education is a fundamental right that should not be limited by financial constraints or geographical boundaries. Our foundation aims to bridge the educational gap by offering comprehensive educational support to students of all ages, from primary to secondary level.
At Prime Education Foundation, we are driven by the belief that every child possesses unique talents and abilities waiting to be nurtured. Through our tailored tuition programs, we provide personalized attention, guidance, and resources that enable students to unlock their full potential and excel academically.
          </p>
          <a href="{{ route('about-us') }}" class="btn-learn-more">Learn More</a>
        </div>
        <div class="col-lg-6 vi">
          <p style="text-align: justify">
            Our vision is to be a Education with excellent level of pedigree with good integrity and reliability in nurturing students to achieve excellence and success.
          </p>
          <ul>
            <li><i class="ri-check-double-line"></i> Grammar Programs</li>
            <li><i class="ri-check-double-line"></i>Afterschool Activities</li>
            <li><i class="ri-check-double-line"></i> Reading Club</li>
          </ul>
        </div>
      </div>

    </div>
  </section><!-- End About Section -->

  <!-- ======= Features Section ======= -->
  <section id="features" class="features">
    <div class="container">

      <ul class="nav nav-tabs row d-flex">
        <li class="nav-item col-3" data-aos="zoom-in">
          <a class="nav-link active show" data-bs-toggle="tab" href="#tab-1">
            <i class="ri-gps-line"></i>
            <h4 class="d-none d-lg-block">Summer School More than 50% Free Lessons</h4>
          </a>
        </li>
        <li class="nav-item col-3" data-aos="zoom-in" data-aos-delay="100">
          <a class="nav-link" data-bs-toggle="tab" href="#tab-2">
            <i class="ri-body-scan-line"></i>
            <h4 class="d-none d-lg-block">KS3 Literacy Programme</h4>
          </a>
        </li>
        <li class="nav-item col-3" data-aos="zoom-in" data-aos-delay="200">
          <a class="nav-link" data-bs-toggle="tab" href="#tab-3">
            <i class="ri-sun-line"></i>
            <h4 class="d-none d-lg-block">KEY STAGE 1 & 2 Literacy/ Numeracy</h4>
          </a>
        </li>
        <li class="nav-item col-3" data-aos="zoom-in" data-aos-delay="300">
          <a class="nav-link" data-bs-toggle="tab" href="#tab-4">
            <i class="ri-store-line"></i>
            <h4 class="d-none d-lg-block">Free SATS Boosters</h4>
          </a>
        </li>
      </ul>

      <div class="tab-content" data-aos="fade-up">
        <div class="tab-pane active show" id="tab-1">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
              
              <h3>How we work</h3>
              {{-- <ul>
                <li><i class="ri-check-double-line"></i> The company mission is to assist and provide cooperation’s with academic schools to improve self-developments and educations to ensure excellent future</li>
                <li><i class="ri-check-double-line"></i>Present employment opportunities to the graduates and high-achieving undergraduate</li>
                <li><i class="ri-check-double-line"></i>Utilizing the expertise of experience teachers/ officers including retirees to contribute to educational benefits to the institution.</li>
                <li><i class="ri-check-double-line"></i>
                  Exercising the proper definition of ‘Tuition School’ which is to help those students which are in need of better understanding on their limitations of certain subjects</li>
                <li><i class="ri-check-double-line"></i>Provide varieties of teaching and learning tools incorporated with relevant theories applies to the academic purposes.</li>
              </ul> --}}
               <ul>
                <li><i class="ri-check-double-line"></i> We work with local children, expert tutors and subject heads to create a bespoke plan that meets the needs of the local children community.</li>
                <li><i class="ri-check-double-line"></i> The target beneficiaries are the children and young people living in the communities where we work. </li>
                <h3>Facts in UK</h3>
                <li><i class="ri-check-double-line"></i> 18% students failed to get a grade 4 in English and Maths.</li>
                <li><i class="ri-check-double-line"></i> 23% fail to reach expected levels of language development by 5.</li>
                <li><i class="ri-check-double-line"></i> 30% of children from deprived families get 5+ good GCSE passes, compared to 70% of peers from wealthier families. </li>
                <li><i class="ri-check-double-line"></i> 1 in 3 children living in poverty fall behind with their education</li>
                <li><i class="ri-check-double-line"></i> Families Connect: Building on feedback from parents looking for support in literacy/language development, numeracy & emotional development.</li>
               </ul>

              <p>
                
              </p>

            

            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center">
              <img src="assets/img/features-1.png" alt="" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab-2">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
              <h3>Key Stages 3 Literacy Project</h3>
              <p>
                Literacy is a fundamental skill that enables students to successfully progress through school, and they can also achieve success in advancing from primary to secondary school and onto adulthood and employment.
                The project Literacy aims to enhance the reading and writing proficiency of all children aged 11–14 years old.

              </p>
              <p class="fst-italic">
                <b>Child-level outcomes</b>
              </p>
              <ul>
                <li><i class="ri-check-double-line"></i> 37% increase in pupils with the highest reading ability.</li>
                <li><i class="ri-check-double-line"></i>56% reduction in days lost due to exclusions (vs-29% nationally) </li>
                <!--
                <li><i class="ri-check-double-line"></i> Provident mollitia neque rerum asperiores dolores quos qui a. Ipsum neque dolor voluptate nisi sed.</li>
                <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
              -->
              </ul>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center">
              <img src="assets/img/Featured_image.jpg" alt="" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab-3">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
              <h3>Voluptatibus commodi ut accusamus ea repudiandae ut autem dolor ut assumenda</h3>
              <p>
                Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                culpa qui officia deserunt mollit anim id est laborum
              </p>
              <ul>
                <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                <li><i class="ri-check-double-line"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                <li><i class="ri-check-double-line"></i> Provident mollitia neque rerum asperiores dolores quos qui a. Ipsum neque dolor voluptate nisi sed.</li>
              </ul>
              <p class="fst-italic">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua.
              </p>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center">
              <img src="assets/img/features-3.png" alt="" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab-4">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
              <h3>Omnis fugiat ea explicabo sunt dolorum asperiores sequi inventore rerum</h3>
              <p>
                Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                culpa qui officia deserunt mollit anim id est laborum
              </p>
              <p class="fst-italic">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua.
              </p>
              <ul>
                <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                <li><i class="ri-check-double-line"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
              </ul>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center">
              <img src="assets/img/features-4.png" alt="" class="img-fluid">
            </div>
          </div>
        </div>
      </div>

    </div>
  </section><!-- End Features Section -->

  <!-- ======= Cta Section ======= -->
  <section id="cta" class="cta">
    <div class="container">

      <div class="row" data-aos="zoom-out">
        <div class="col-lg-9 text-center text-lg-start">
          <h3>Call To Action</h3>
          <p> PRIME Education IS OFFERING TUITION TO ALL STUDENTS</p>
        </div>
        <div class="col-lg-3 cta-btn-container text-center">
          <a class="cta-btn align-middle" href="#">Contact Us</a>
        </div>
      </div>

    </div>
  </section><!-- End Cta Section -->

  <!-- ======= Services Section ======= -->
  <section id="services" class="services">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Services</h2>
        <p>What we do offer</p>
      </div>
<
      <div class="row">
        <div class="col-lg-4 col-md-6">
          <div class="icon-box" data-aos="zoom-in-left">
            <div class="icon"><i class="bi bi-briefcase" style="color: #ff689b; margin-left: 30px"></i></div>
            <div style="margin-left: 10px;margin-right: 30px">
            <h4 class="title"><a href="">Tuition and Exam Coaching</a></h4>
            <p class="description">Tuition and Exam Coaching Support based at Prime Education Campus. Tuition available for all ages and abilities including GCSE and A Level in all mainstream UK subjects</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mt-5 mt-md-0">
          <div class="icon-box" data-aos="zoom-in-left" data-aos-delay="100">
            <div class="icon"><i class="bi bi-book" style="color: #e9bf06;margin-left: 30px"></i></div>
            <h4 class="title"><a href="">Free Booster Lessons to improve Exam preparation</a></h4>
            <div style="margin-left: 10px;margin-right: 30px">
            <p class="description">Our Revision Schools help small groups of A Level and GCSE students prepare for upcoming exams and improve performance. Each session is tailored to the subject, exam board and individual student’s requirements. Sessions are run by our qualified tutors.</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 mt-5 mt-lg-0 ">
          <div class="icon-box" data-aos="zoom-in-left" data-aos-delay="200">
            <div class="icon"><i class="bi bi-card-checklist" style="color: #3fcdc7;margin-left: 30px"></i></div>
            <h4 class="title"><a href="">Support for additional need learners</a></h4>
            <div style="margin-left: 10px;margin-right: 30px">            
            <p class="description">Tailored and intensive programme for help young pupils to fulfill their potential.<br/>This programme provide support at individual level for each student.<br/>
              <b>Offered at less than half price</b></p>
            </div>  
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mt-5">
          <div class="icon-box" data-aos="zoom-in-left" data-aos-delay="300">
            <div class="icon"><i class="bi bi-binoculars" style="color:#41cf2e;margin-left: 30px"></i></div>
            <h4 class="title"><a href="">Building Vocabulary in KS1, KS2</a></h4>
           <div style="margin-left: 10px;margin-right: 30px">            
            <p class="description">This course helps primary students develop their knowledge, reading and Vocabulary how they work together with group activities.</p>
           </div> 
          </div>
        </div>

        <div class="col-lg-4 col-md-6 mt-5">
          <div class="icon-box" data-aos="zoom-in-left" data-aos-delay="400">
            <div class="icon"><i class="bi bi-globe" style="color: #d6ff22;margin-left: 30px"></i></div>
            <h4 class="title"><a href="">Free SATS Lessons</a></h4>
           <div style="margin-left: 10px;margin-right: 30px">            
            <p class="description">At Prime Education, your child will progress through the individualized instruction at his or her own pace – helping students achieve target scores in Literacy and Numeracy</p>
           </div> 
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mt-5">
          <div class="icon-box" data-aos="zoom-in-left" data-aos-delay="500">
            <div class="icon"><i class="bi bi-clock" style="color: #4680ff;margin-left: 30px"></i></div>
            <h4 class="title"><a href="">Adult Literacy Support Programme</a></h4>
            <div style="margin-left: 10px;margin-right: 30px">            
            <p class="description">This programme help adults and especially to improve their literacy and mathematical skill to improve their communication skills and help them build confidence in society.</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 mt-5">
          <div class="icon-box" data-aos="zoom-in-left" data-aos-delay="500">
            <div class="icon"><i class="bi bi-bounding-box" style="color: #3fcdc7;margin-left: 30px"></i></div>
            <h4 class="title"><a href="">Improving Writing in Key Stage 2</a></h4>
            <div style="margin-left: 10px;margin-right: 30px">            
            <p class="description">Participating students made approximately six months' additional progress compared to similar pupils who did not participate.</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 mt-5">
          <div class="icon-box" data-aos="zoom-in-left" data-aos-delay="500">
            <div class="icon"><i class="bi bi-file-ruled-fill" style="color: #41cf2e;margin-left: 30px"></i></div>
            <h4 class="title"><a href="">Mentoring programmes</a></h4>
            <div style="margin-left: 10px;margin-right: 30px">            
            <p class="description">Our mentoring projects provide one-to-one support to young people to develop their confidence and motivation.</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 mt-5">
          <div class="icon-box" data-aos="zoom-in-left" data-aos-delay="500">
            <div class="icon"><i class="bi bi-sun-fill" style="color: yellow;margin-left: 30px"></i></div>
            <h4 class="title"><a href="">Key Stages 3 Literacy Project</a></h4>
            <div style="margin-left: 10px;margin-right: 30px">            
            <p class="description">The project Literacy aims to enhance the reading and writing proficiency of all children aged 11–14 years old.</p>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section><!-- End Services Section -->

  <!-- ======= Team Section ======= -->
  <section id="team" class="team">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Result</h2>
        <p>Our Overall Result</p>
      </div>

      <div class="row">

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch" style="background-color: #f07014">
          <div class="member" data-aos="fade-up" data-aos-delay="200" >
            <div class="member-img">
              <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="" >
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h2 style="text-align: center;">71%</h2>
              <h4 style="text-align: center; color: #f07014" >passed with improvement Grades 7 and above local community students.</h4>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch" style="background-color: #f07014">
          <div class="member" data-aos="fade-up" data-aos-delay="200">
            <div class="member-img">
              <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h2 style="text-align: center;">29%</h2>
              <h4 style="text-align: center; color: #f07014" >Passed with Grades 4 and above.</h4>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch" style="background-color: #f07014">
          <div class="member" data-aos="fade-up" data-aos-delay="200">
            <div class="member-img">
              <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h2 style="text-align: center;">92%</h2>
              <h4 style="text-align: center; color: #f07014" >performance in learning outcome of</h4>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch" style="background-color: #f07014">
          <div class="member" data-aos="fade-up" data-aos-delay="200">
            <div class="member-img">
              <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h2 style="text-align: center;">96%</h2>
              <h4 style="text-align: center; color: #f07014" >SATS RESULT ABOVE STADARD</h4>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section><!-- End Team Section -->

  <!-- ======= Portfolio Section ======= -->
{{--   <section id="portfolio" class="portfolio">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Portfolio</h2>
        <p>What we've done</p>
      </div>

      <ul id="portfolio-flters" class="d-flex justify-content-end" data-aos="fade-up">
        <li data-filter="*" class="filter-active">All</li>
        <li data-filter=".filter-app">App</li>
        <li data-filter=".filter-card">Card</li>
        <li data-filter=".filter-web">Web</li>
      </ul>

      <div class="row portfolio-container" data-aos="fade-up">

        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-img"><img src="assets/img/portfolio/portfolio-1.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>App 1</h4>
            <p>App</p>
            <a href="assets/img/portfolio/portfolio-1.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
          <div class="portfolio-img"><img src="assets/img/portfolio/portfolio-2.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Web 3</h4>
            <p>Web</p>
            <a href="assets/img/portfolio/portfolio-2.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-img"><img src="assets/img/portfolio/portfolio-3.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>App 2</h4>
            <p>App</p>
            <a href="assets/img/portfolio/portfolio-3.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 2"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
          <div class="portfolio-img"><img src="assets/img/portfolio/portfolio-4.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Card 2</h4>
            <p>Card</p>
            <a href="assets/img/portfolio/portfolio-4.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 2"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
          <div class="portfolio-img"><img src="assets/img/portfolio/portfolio-5.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Web 2</h4>
            <p>Web</p>
            <a href="assets/img/portfolio/portfolio-5.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 2"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-img"><img src="assets/img/portfolio/portfolio-6.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>App 3</h4>
            <p>App</p>
            <a href="assets/img/portfolio/portfolio-6.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 3"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
          <div class="portfolio-img"><img src="assets/img/portfolio/portfolio-7.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Card 1</h4>
            <p>Card</p>
            <a href="assets/img/portfolio/portfolio-7.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 1"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
          <div class="portfolio-img"><img src="assets/img/portfolio/portfolio-8.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Card 3</h4>
            <p>Card</p>
            <a href="assets/img/portfolio/portfolio-8.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 3"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
          <div class="portfolio-img"><img src="assets/img/portfolio/portfolio-9.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Web 3</h4>
            <p>Web</p>
            <a href="assets/img/portfolio/portfolio-9.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

      </div>

    </div>
  </section> --}}<!-- End Portfolio Section -->



{{-- <!-- New Section -->
  <section id="cta-second" class="cta">
    <div class="container">

      <div class="row" data-aos="zoom-out">
<!--
          <h3>Call To Action</h3>
-->          
        <center>
          <p style="font-weight: 400; font-size: 24px">Working with Parents to Support Children’s Learning because Every child deserves the right to succeed</p>
        </center>

<!--        
        <div class="col-lg-3 cta-btn-container text-center">
          <a class="cta-btn align-middle" href="#">Contact Us</a>
        </div>
-->        
      </div>

    </div>
  </section><!-- End Cta Section --> --}}

<!--Facts in UK-->
 <section id="team" class="team">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Facts</h2>
        <p>in UK</p>
      </div>

      <div class="row">

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch" style="background-color: #f07014">
          <div class="member" data-aos="fade-up" data-aos-delay="200" >
            <div class="member-img">
              <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="" >
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h2 style="text-align: center;">18%</h2>
              <h4 style="text-align: center; color: #f07014" >students failed to get a grade 4 in English and Maths</h4>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch" style="background-color: #f07014">
          <div class="member" data-aos="fade-up" data-aos-delay="200">
            <div class="member-img">
              <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h2 style="text-align: center;">23%</h2>
              <h4 style="text-align: center; color: #f07014" >fail to reach expected levels of language development by 5</h4>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch" style="background-color: #f07014">
          <div class="member" data-aos="fade-up" data-aos-delay="200">
            <div class="member-img">
              <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h2 style="text-align: center;">30%</h2>
              <h4 style="text-align: center; color: #f07014" >of children from deprived families get 5+ good GCSE passes, compared to 70% of peers from wealthier families.</h4>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch" style="background-color: #f07014">
          <div class="member" data-aos="fade-up" data-aos-delay="200">
            <div class="member-img">
              <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h2 style="text-align: center;">1 in 3</h2>
              <h4 style="text-align: center; color: #f07014" >children living in poverty fall behind with their education</h4>
            </div>
          </div>
        </div>


      </div>

      <div class="row content" style="background-color: #f07014">
        <div  data-aos="fade-up">
          <div class="col-lg-12 text-white" >
            <ul class="list-disc">
              <li><p>Families Connect:  <b>Building on feedback from parents</b> looking for support in literacy/language development, numeracy & emotional development</p></li>
              <li><p> Our programmes and services aim to support all of those involved in educating young people.</p></li>
            </ul>
          </div>
        </div>
      </div>    

    </div>
  </section><!-- End Team Section -->

  <!-- ======= Testimonials Section ======= -->
  <section id="testimonials" class="testimonials">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Testimonials</h2>
        <p>What Parent's are saying about us</p>
      </div>

      <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
        <div class="swiper-wrapper">

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  I cannot express my gratitude enough for the incredible support and educational opportunities provided by the Prime Education Foundation. They have not only offered extra free lessons to my child in need of support but has also played a significant role in helping him achieve exceptional academic success.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Saida Shirwa</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->


          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Prime Education Foundation is an exceptional organization. They offered half price booster lessons support for my child's A-level exams, and the results were remarkable. The tutors were not only highly qualified but also caring and dedicated. The personalized attention and tailored programs made a significant impact on my child's understanding of the subjects. Prime Education truly goes the extra mile to help students succeed!
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Rose Green</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  I can't thank Prime Education enough for their outstanding assistance during my child's GCSE exams. The they provided were of the highest quality, and the tutors were patient and knowledgeable. My child's grades improved significantly, and their confidence skyrocketed. Prime Education Foundation is a blessing for families who are deprived and can’t afford expensive tuition but want their children to excel academically.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>John Doe</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  I am extremely grateful to Prime Education for their outstanding support during my child's GCSE and A-level exams. The discounted tuition lessons they provided were incredibly beneficial and their tutors were supportive. Thanks to Prime Education's guidance and resources, my child achieved excellent grades that surpassed our expectations. I can't thank them enough for the positive impact they've made on my child's education.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Fang</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->


          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Prime Education has been a tremendous support for my child's SATS and literacy skills development. The free lessons they offer are outstanding and have greatly improved my child's understanding and confidence. The tutors are patient, and dedicated. Prime Education's commitment to providing quality education for all children is commendable.             I highly appreciate Prime Education to support local parents seeking support for their children in SATS and literacy skills.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Helen</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Prime Education Foundation has been a true blessing for me in improving my literacy and numeracy skills. The free programs they offer are comprehensive, engaging, and have greatly boosted my confidence. The tutors are passionate, patient, and dedicated to helping individuals succeed. Thanks to Prime Education, my reading and math abilities have improved significantly. I am grateful for their commitment to empowering individuals through education
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
<!--
              <h3>Helen</h3>
              <h4>Parent</h4>
-->              
            </div>
          </div><!-- End testimonial item -->


          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Prime Education has been an incredible resource for adults seeking literacy support in our community. The free programs they offer are outstanding, and the tutors are compassionate, knowledgeable, and skilled at helping adults improve their literacy skills. Thanks to Prime Education, I have gained confidence in reading and writing, which has positively impacted my personal and professional life. I am deeply grateful for their dedication and support. I highly recommend Prime Education to adults seeking literacy assistance.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Paulina</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Prime Education has been a lifeline for adults and parents in our local community seeking literacy and numeracy support. The programs they offer are significantly discounted but comprehensive and tailored to individual needs. The tutors are patient, understanding, and skilled in helping adults improve their literacy skills. Thanks to Prime Education, I have gained confidence in reading and writing, and my overall literacy skills have improved significantly. I am grateful for their dedication to empowering adults through education.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>M Buaro</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->



{{--
          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                My Son has Got an Exceptional GCSE Results range from 9 To 7 in all subjects with support of Prime Education. Prime Education is proved to be excellent in teaching and resources they provide to my child. I highly appreciate them and recommended prime education to other parents.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Caroline</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Excellent in every respect, teaching, conduct, respectful, give attention to details and follow-ups to students work and also very clean environment. Very safe as well. Thank you and keep on the God job.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="">
              <h3>Peterson</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                My daughter joined you in February to prepare for Grammer Schools Selective Exams in Sept. She has never been taught Verbal and Non Verbal Reasoning before and it was difficult for her at the beginning but you were able to break it down for her and she started enjoying it
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="">
              <h3>Anita</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                I have both my daughters enrolled in Prime Education. I have found the tutors to be patient, knowledgeable, and caring. My eldest daughter is finding the Year 12 Chemistry an enormous help, especially with her exam preparation. Her marks have improved dramatically from last year.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt="">
              <h3>Melinda</h3>
              <h4>Parent</h4>
            </div>
          </div><!-- End testimonial item -->
--}}

         {{--  <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt="">
              <h3>John Larson</h3>
              <h4>Entrepreneur</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Prime Education has been an incredible resource for adults seeking literacy support in our community. The free programs they offer are outstanding, and the tutors are compassionate, knowledgeable, and skilled at helping adults improve their literacy skills. Thanks to Prime Education, I have gained confidence in reading and writing, which has positively impacted my personal and professional life. I am deeply grateful for their dedication and support. I highly recommend Prime Education to adults seeking literacy assistance.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>Paulina</h3>
              <h4>Adult Learning Reviews</h4>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Prime Education has been a lifeline for adults and parents in our local community seeking literacy and numeracy support. The programs they offer are significantly discounted but comprehensive and tailored to individual needs. The tutors are patient, understanding, and skilled in helping adults improve their literacy skills. Thanks to Prime Education, I have gained confidence in reading and writing, and my overall literacy skills have improved significantly. I am grateful for their dedication to empowering adults through education.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
              <h3>M Buaro</h3>
              <h4>Adult Learning Reviews</h4>
            </div>
          </div><!-- End testimonial item -->




        </div>
        <div class="swiper-pagination"></div> --}}
      </div>

    </div>
  </section><!-- End Testimonials Section -->


  <!-- ======= Testimonials Section ======= -->


  <!-- ======= Pricing Section ======= -->
  {{-- <section id="pricing" class="pricing">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Pricing</h2>
        <p>Our Competing Prices</p>
      </div>

      <div class="row">

        <div class="col-lg-3 col-md-6">
          <div class="box" data-aos="zoom-in">
            <h3>Free</h3>
            <h4><sup>$</sup>0<span> / month</span></h4>
            <ul>
              <li>Aida dere</li>
              <li>Nec feugiat nisl</li>
              <li>Nulla at volutpat dola</li>
              <li class="na">Pharetra massa</li>
              <li class="na">Massa ultricies mi</li>
            </ul>
            <div class="btn-wrap">
              <a href="#" class="btn-buy">Buy Now</a>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
          <div class="box featured" data-aos="zoom-in" data-aos-delay="100">
            <h3>Business</h3>
            <h4><sup>$</sup>19<span> / month</span></h4>
            <ul>
              <li>Aida dere</li>
              <li>Nec feugiat nisl</li>
              <li>Nulla at volutpat dola</li>
              <li>Pharetra massa</li>
              <li class="na">Massa ultricies mi</li>
            </ul>
            <div class="btn-wrap">
              <a href="#" class="btn-buy">Buy Now</a>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
          <div class="box" data-aos="zoom-in" data-aos-delay="200">
            <h3>Developer</h3>
            <h4><sup>$</sup>29<span> / month</span></h4>
            <ul>
              <li>Aida dere</li>
              <li>Nec feugiat nisl</li>
              <li>Nulla at volutpat dola</li>
              <li>Pharetra massa</li>
              <li>Massa ultricies mi</li>
            </ul>
            <div class="btn-wrap">
              <a href="#" class="btn-buy">Buy Now</a>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
          <div class="box" data-aos="zoom-in" data-aos-delay="300">
            <span class="advanced">Advanced</span>
            <h3>Ultimate</h3>
            <h4><sup>$</sup>49<span> / month</span></h4>
            <ul>
              <li>Aida dere</li>
              <li>Nec feugiat nisl</li>
              <li>Nulla at volutpat dola</li>
              <li>Pharetra massa</li>
              <li>Massa ultricies mi</li>
            </ul>
            <div class="btn-wrap">
              <a href="#" class="btn-buy">Buy Now</a>
            </div>
          </div>
        </div>

      </div>

    </div>
  </section> --}}<!-- End Pricing Section -->

  <!-- ======= F.A.Q Section ======= -->
 {{--  <section id="faq" class="faq">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>F.A.Q</h2>
        <p>Frequently Asked Questions</p>
      </div>

      <ul class="faq-list">

        <li>
          <div data-bs-toggle="collapse" class="collapsed question" href="#faq1">How much do lessons cost? <i class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i></div>
          <div id="faq1" class="collapse" data-bs-parent=".faq-list">
            <p>
              We do our best to offer customers value for money. We have a variety of packages available based on the number of subjects and lessons you enroll on too. An average hourly cost is around £5.50 Per Hour to 15 Per Hour from KS1 to University level students.
            </p>
          </div>
        </li>

        <li>
          <div data-bs-toggle="collapse" href="#faq2" class="collapsed question">Do you offer 1-1 or group lessons? <i class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i></div>
          <div id="faq2" class="collapse" data-bs-parent=".faq-list">
            <p>
              We offer options of both.
            </p>
          </div>
        </li>

        <li>
          <div data-bs-toggle="collapse" href="#faq3" class="collapsed question">Which ages do you teach?
            <i class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i></div>
          <div id="faq3" class="collapse" data-bs-parent=".faq-list">
            <p>
              Children aged 5 years up to Undergraduate level.
            </p>
          </div>
        </li>

        <li>
          <div data-bs-toggle="collapse" href="#faq4" class="collapsed question">Which days and times are the lessons? <i class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i></div>
          <div id="faq4" class="collapse" data-bs-parent=".faq-list">
            <p>
              Our lessons run on weekday evenings from 4:45pm -9:00pm and on weekends from 09:00am to 06:30pm both Saturday & Sunday. Once we have completed the initial assessment & consultation, your child will be allocated to suitable classes based on their age and academic ability. Students are split into groups, like sets in schools. We have a number of groups available during the weekday evenings and weekends.
            </p>
          </div>
        </li>

        <li>
          <div data-bs-toggle="collapse" href="#faq5" class="collapsed question">Are you Ofsted registered? <i class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i></div>
          <div id="faq5" class="collapse" data-bs-parent=".faq-list">
            <p>
              Yes, see our Ofsted page for more information on what this means and how it might be able to benefit you.
            </p>
          </div>
        </li>

      </ul>

    </div>
  </section> --}}<!-- End F.A.Q Section -->

  <!-- ======= Contact Section ======= -->
  <section id="contact" class="contact">
    <div class="container">

      <div class="section-title" data-aos="zoom-out">
        <h2>Contact</h2>
        <p>Contact Us</p>
      </div>

      <div class="row mt-5">

        <div class="col-lg-4" data-aos="fade-right">
          <div class="info">
            <div class="address">
              <i class="bi bi-geo-alt"></i>
              <h4>Location:</h4>
              <p>10 Acre House, Acre Lane<br>
                (Opposite Lambeth Town Hall)<br>
                Brixton London SW2 5SG.</p>
            </div>

            <div class="email">
              <i class="bi bi-envelope"></i>
              <h4>Email:</h4>
              <p>info@prime-education.org.uk</p>
            </div>

            <div class="phone">
              <i class="bi bi-phone"></i>
              <h4>Call:</h4>
              <p>0203 302 1524</p>
            </div>

          </div>

        </div>

        <div class="col-lg-8 mt-5 mt-lg-0" data-aos="fade-left">

          <form action="{{route('contact')}}" id="contact-form" method="post" role="form" class="php-email-form">
            @csrf
            <div class="row">
              <div class="col-md-6 form-group">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
              </div>
              <div class="col-md-6 form-group mt-3 mt-md-0">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
              </div>
            </div>
            <div class="form-group mt-3">
              <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
            </div>
            <div class="form-group mt-3">
              <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
            </div>
            <div class="my-3">
              <div class="sent-message">Your message has been sent. Thank you!</div>
            </div>
            <div class="text-center"><button type="submit" >Send Message</button></div>
          </form>

        </div>

      </div>

    </div>
  </section><!-- End Contact Section -->
  <script src="assets/js/code.jquery.com_jquery-3.7.0.min.js"></script>

  <script type="text/javascript">
$(document).ready(function() {
    // show the alert
    setTimeout(function() {
        $(".alert").alert('close');
    }, 3000);
});
  </script>
  @endsection