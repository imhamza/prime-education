<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactFormController;
use App\Http\Controllers\OnlineApplicationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('frontend');
//.......................................
/* Route::get('/about-us/our-mission', function () {
    return view('pages.about.mission');
})->name('our_mission');
Route::get('/about-us/our-vision', function () {
    return view('pages.about.vision');
})->name('our_vision');
Route::get('/about-us/our-team', function () {
    return view('pages.about.team');
})->name('our_team'); */
/* Route::get('/about-us', function () {
    return view('pages.about.about');
})->name('about'); */
//...................................

Route::get('/parent-s-comment', function () {
    return view('pages.comments.parents_comments');
})->name('parent_comment');
Route::get('/education-benefits', function () {
    return view('pages.comments.education_benefits');
})->name('education_benefits');

//.....................................
Route::get('/services', function () {
    return view('pages.services.service');
})->name('service');
//................................
/* Route::get('awarding-bodies/ofsted', function () {
    return view('pages.awarding.ofsted');
})->name('ofsted');
Route::get('awarding-bodies/ocr', function () {
    return view('pages.awarding.ocr');
})->name('ocr');
Route::get('awarding-bodies/edexcel', function () {
    return view('pages.awarding.edexcel');
})->name('edexcel'); */
/* Route::get('/awarding-bodies', function () {
    return view('pages.awarding.awarding');
})->name('awarding'); */

//...................................
Route::get('/careers', function () {
    return view('pages.careers.career');
})->name('career');

Route::get('/contact-us', function () {
    return view('pages.contact.contact');
})->name('contact-us');

Route::get('/about-us', function(){
    return view('pages.about.about_us');
})->name('about-us');

Route::get('/online_application', function(){
    return view('pages.online_application.form');
})->name('online_application');

Route::post('/contact',[ContactFormController::class,'ContactStore'])->name('contact');

Route::get('get_student_fields/{id}', [OnlineApplicationController::class,'add_subject']);

Route::post('/online_application', [OnlineApplicationController::class,'online_application_store']);

Route::get('/online_application_thanks', function(){
    return view('pages.online_application.thanks');
});